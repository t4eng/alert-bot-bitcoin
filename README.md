1. **npm install** 


2. **touch env.dev** 
  - LINE_CHANNEL_TOKEN=<<Channel_Access_Token>>
  - LINE_CHANNEL_SECRET=<<Channel_Secret>>
  - GROUP_ID=<<Group_Id>>
  - LINE_PUSH_MESSAGE=https://api.line.me/v2/bot/message/push
  - API_CURRENCY=https://api.bitkub.com/api/market/ticker?fbclid=IwAR3Eg6_vweArlMcS6cwZnKXSYXTH6pGxqc9XJOMRjFjIHo6W_AeiabTZK0o
  - DATABASE_URL=<<Database_Url_Firebase>>

3. **touch serviceAccountKey.json** 
  Download serviceAccountKey.json
  - https://console.firebase.google.com/project/<<Id_Project>>/settings/serviceaccounts/adminsdk