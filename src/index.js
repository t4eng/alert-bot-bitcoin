import express from 'express'
import { ENV, TEXT, sendMessageToUser, sendMessageToGroup } from './utils'
const PORT = process.env.PORT || 8445
const app = express()
app.use(express.json({ limit: '10mb' }))

app.post('/webhook', async (req, res) => {
  req.body.events.map(event => {
    if (TEXT.INITIAL_REPLYTOKEN.includes(event.replyToken)) {
      //check set webhook
      console.log(event)
    } else {
      //event chatbot
      console.log(event)
      sendMessageToUser(event.source.userId, event.message)
    }
  })
  res.status(200).end('')
})

app.listen(PORT, () => console.log(`🚀 server http://localhost:${PORT}`))
