require('../config/dotenv.config')

export const ENV = {
  LINE_CHANNEL_TOKEN: process.env.LINE_CHANNEL_TOKEN,
  LINE_CHANNEL_SECRET: process.env.LINE_CHANNEL_SECRET,
  API_CURRENCY: process.env.API_CURRENCY,
  GROUP_ID: process.env.GROUP_ID,
  LINE_PUSH_MESSAGE: process.env.LINE_PUSH_MESSAGE,
  DATABASE_URL: process.env.DATABASE_URL
}
