import axios from 'axios'
import { ENV } from '.'

const headers = {
  'Content-Type': 'application/json',
  Authorization: `Bearer ${ENV.LINE_CHANNEL_TOKEN}`
}

export const sendMessageToGroup = messages => {
  messages = Array.isArray(messages) ? messages : [messages]
  const body = { to: ENV.ID_GROUP, messages: messages }

  axios({ method: 'post', url: ENV.LINE_PUSH_MESSAGE, headers, data: body })
}
