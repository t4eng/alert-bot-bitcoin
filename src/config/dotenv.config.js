import * as dotenv from 'dotenv'
import * as process from 'process'

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

if (process.env.NODE_ENV === 'development') {
  dotenv.config({ path: '.env.dev' })
}
