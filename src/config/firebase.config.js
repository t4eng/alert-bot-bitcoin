import firebase from 'firebase-admin'
import serviceAccount from '../../serviceAccountKey.json'
import { ENV } from '../utils'

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: ENV.DATABASE_URL
})

export default firebase
